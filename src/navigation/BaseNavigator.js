import React, { Component } from 'react';
import { Text, Image, View, Dimensions } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Login from '../Component/Login/Login';
import SplashScreen from '../Component/Login/SplashScreen';
import SideMenu from '../Component/SideMenu/SideMenu';
import Dashboard from '../Component/Dashboard/Dashboard';
import AddCompany from '../Component/AddCompany/AddCompany';
import AddJobType from '../Component/AddJobType/AddJobType';
import NewOrder from '../Component/NewOrder/NewOrder';
import CompleteOrder from '../Component/CompletedOrder/CompletedOrder';
import UpdateOrder from '../Component/Dashboard/UpdateOrder';
import AllInvoices from '../Component/AllInvoices/AllInvoices';

import { Routes } from "./Routes";

import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
    'Warning: componentWillMount is deprecated',
    'Warning: componentWillUpdate is deprecated',
    'Warning: componentWillReceiveProps is deprecated',
    'Module RCTImageLoader requires',
    'Warning: Async Storage has been extracted from react-native core'
]);

const { width, height } = Dimensions.get('window');

const DashboardStack = createStackNavigator({
    [Routes.Dashboard]: { screen: Dashboard },
    [Routes.UpdateOrder]: { screen: UpdateOrder },
    [Routes.NewOrder]: { screen: NewOrder },

}, {
    headerMode: 'none'
});

const AddCompanyStack = createStackNavigator({
    [Routes.AddCompany]: {
        screen: AddCompany,
    }

}, {
    headerMode: 'none'
});

const AddJobTypeStack = createStackNavigator({
    [Routes.AddJobType]: {
        screen: AddJobType,
    }

}, {
    headerMode: 'none'
});

const NewOrderStack = createStackNavigator({
    [Routes.NewOrder]: {
        screen: NewOrder,
    }

}, {
    headerMode: 'none'
});

const CompleteOrderStack = createStackNavigator({
    [Routes.CompleteOrder]: {
        screen: CompleteOrder,
    }

}, {
    headerMode: 'none'
});

const AllInvoicesStack = createStackNavigator({
    [Routes.AllInvoices]: {
        screen: AllInvoices,
    }

}, {
    headerMode: 'none'
});

const DrawerStack = createDrawerNavigator(
    {
        [Routes.DashboardStack]: {
            screen: DashboardStack,
        },
        [Routes.AddCompanyStack]: {
            screen: AddCompanyStack,
            navigationOptions: {
                drawerLockMode: "locked-closed",
                disableGestures: true
            }
        },
        [Routes.AddJobTypeStack]: {
            screen: AddJobTypeStack,
            navigationOptions: {
                drawerLockMode: "locked-closed",
                disableGestures: true
            }
        },
        // [Routes.NewOrderStack]: {
        //     screen: NewOrderStack,
        //     navigationOptions: {
        //         drawerLockMode: "locked-closed",
        //         disableGestures: true
        //     }
        // },
        // [Routes.CompleteOrderStack]: {
        //     screen: CompleteOrderStack,
        //     navigationOptions: {
        //         drawerLockMode: "locked-closed",
        //         disableGestures: true
        //     }
        // },
        [Routes.AllInvoicesStack]: {
            screen: AllInvoicesStack,
            navigationOptions: {
                drawerLockMode: "locked-closed",
                disableGestures: true
            }
        },
    }, {
    contentComponent: SideMenu,
    drawerWidth: 280,
    drawerPosition: 'left',

});

const BaseNavigatorContainerFromLogin = createAppContainer(
    createStackNavigator({
        [Routes.SplashScreen]: {
            screen: SplashScreen,
        },
        [Routes.Login]: {
            screen: Login
        },
        
        [Routes.Drawer]: {
            screen: DrawerStack
        },

    }, {
        headerMode: 'none'
    })
);


class BaseNavigator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            BadgeCount: 0
        }
    }

    render() {

        return (
            <BaseNavigatorContainerFromLogin />
        )
    }
}

export { BaseNavigator };