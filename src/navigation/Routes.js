const Routes = {
    Login: 'LOGIN',
    SplashScreen: 'SPLASH_SCREEN',
    Dashboard: 'DASHBOARD',
    Drawer: 'DRAWER',
    AddCompany: 'ADD_COMPANY',
    AddCompanyStack: 'ADD_COMPANY_STACK',
    AddJobType: 'ADD_JOB_TYPE',
    AddJobTypeStack: 'ADD_JOB_TYPE_STACK',
    NewOrderStack: 'NEW_ORDER_STACK',
    NewOrder: 'NEW_ORDER',
    CompleteOrderStack: 'COMPLETE_ORDER_STACK',
    CompleteOrder: 'COMPLETE_ORDER',
    UpdateOrder: 'UPDATE_ORDER',
    AllInvoicesStack: 'ALL_INVOICES_STACK',
    AllInvoices: 'ALL_INVOICES'
};


export { Routes };