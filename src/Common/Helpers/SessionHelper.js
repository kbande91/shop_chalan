import AsyncStorage from '@react-native-community/async-storage';

export const setAsyncAccessToken = async (value) =>{
  await AsyncStorage.setItem('AccessToken', value);
}

export const setAsyncRefreshToken = async (value) =>{
  await AsyncStorage.setItem('RefreshToken', value);
}

export const setAsyncSaveUsername = async (value) =>{
  await AsyncStorage.setItem('Username', value);
}

export const setAsyncSavePassword = async (value) =>{
  await AsyncStorage.setItem('Password', value);
}

export const getAsyncAccessToken = async() =>{
  return await AsyncStorage.getItem('AccessToken');
}

export const getAsyncRefreshToken = async() =>{
  return await AsyncStorage.getItem('RefreshToken');
}

export const getAsyncUsername = async() =>{
  return await AsyncStorage.getItem('Username');
}

export const getAsyncPassword = async() =>{
  return await AsyncStorage.getItem('Password');
}