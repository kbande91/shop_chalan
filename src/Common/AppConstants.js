const AppConstants = {

    //APP CONSTANTS

    // API URLs
    // API_URL: 'https://vri.lta.com.fj/', //UAT
    // API_URL: 'https://vrenf.lta.com.fj/', //UAT ENF
    API_URL: 'http://121.242.77.249:4004/', //QA_VRI
    
    API_GetSpecificationDetails: 'api/v1/VehicleRegInspection/GetSpecificationDetails?',

    //Font_Family
    regularFont: 'NunitoSans-Regular',
    mediumFont: 'NunitoSans-SemiBold',
    boldFont: 'NunitoSans-Bold',
    extraBold: 'NunitoSans-ExtraBold',
    blockFont: 'NunitoSans-Black',

    bacisColor: '#24a19c',
    secondaryColor: '#6ebfb5',
    backgroundColor: '#efefef',
    pendingOrder: '#f0a500',

    //Font_Size
    infolabelSize: 10,
    labelSize: 12,
    infoSize: 14,
    inputSize: 16,
    titleSize: 18,
    buttonSize: 20,
    headerSize: 22,
    
}

export default AppConstants;