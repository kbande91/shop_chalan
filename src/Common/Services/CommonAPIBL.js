import moment from 'moment';

export function getShortDate(ConvertToShortdate) {

  if (ConvertToShortdate != null && ConvertToShortdate != '') {

    // let ShortDate = moment(ConvertToShortdate).format('DD MMMM YYYY')
    let ShortDate = moment(ConvertToShortdate, "YYYY-MM-DD'T'HH:mm:ss").format('DD/MM/YYYY')


    return ShortDate;
  }
  return ConvertToShortdate;
}
