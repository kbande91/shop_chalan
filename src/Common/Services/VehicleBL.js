import { fetchWrapper, ApiError } from '../../Common/Helpers/fetchAPIWrapper';
import AppConstants from '../AppConstants';
import Errorhandler from '../Errorhandler';

import { getAsyncAccessToken, } from '../Helpers/SessionHelper';

export function getVehicleDetails() {

    let url = AppConstants.API_URL + AppConstants.API_GetVehicleDetails;

    return fetchWrapper(url, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
        .then(res => {
            return res.ActionResult;
        }).catch(error => {
            const newError = new Errorhandler(error, "Error: While fetching Vehicle Details");
            newError.showAlert();
        });
};


export function getVehicleSpecifications(VehicleID) {

    let url = AppConstants.API_URL + AppConstants.API_GetVehicleSpecifications + VehicleID;

    return fetchWrapper(url, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
        .then(res => {
            return res.ActionResult;
        }).catch(error => {
            const newError = new Errorhandler(error, "Error: While fetching Vehicle Specification Details");
            newError.showAlert();
        });
};

export function getVehicleModifiedDetails(VehicleID) {

    let url = AppConstants.API_URL + AppConstants.API_GetVehicleModifiedDetails + VehicleID;

    return fetchWrapper(url, { method: 'GET', headers: { 'Content-Type': 'application/json' } })
        .then(res => {
            return res.ActionResult;
        }).catch(error => {
            const newError = new Errorhandler(error, "Error: While fetching Vehicle Modification Details");
            newError.showAlert();
        });
};


export function uploadVehiclePhoto(body) {

    let url = AppConstants.API_URL + AppConstants.API_UploadImageDetails;
    let Accesstoken = getAsyncAccessToken();

    return fetchWrapper(url, {
        method: 'POST',
        headers: { "Accesstoken": Accesstoken },
        body: body,
    })
        .then((res) => res.json())
        .then((res) => {
            console.log("response" + JSON.stringify(res));
            return res;
        })
        .catch(error => {
            const newError = new Errorhandler(error, "Error: While uploading Vehicle image");
            newError.showAlert();
        });
};

