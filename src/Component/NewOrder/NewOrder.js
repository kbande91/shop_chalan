import React, { Fragment } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Text,
    FlatList,
    TextInput,
    ScrollView,
    Keyboard,
    Platform
} from 'react-native';
import styles from './style';
import AppConstants from '../../Common/AppConstants';
import { SafeAreaView } from 'react-navigation';
import { Header } from '../common';
import { Routes } from '../../navigation/Routes';
import CustomAlert from '../SideMenu/CustomAlert';
import NetInfo from "@react-native-community/netinfo";
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import DateTimePicker from "react-native-modal-datetime-picker";
import database from '@react-native-firebase/database';

const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');
var DeviceInfo = require('react-native-device-info');

class NewOrder extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
            CompanyName: 'Select Company',
            CompanyNameList: [],
            GST_no: '',
            jobType: 'Select Job Type',
            jobTypeData: [],
            WorkType: 'Select Work Type',
            workTypeData: [{value: 'For CNC'}, {value: 'For Re-work'}],
            quantity: '',
            modalVisible: false,
            PONo: '',
            PODate: 'P.O. Date',
            ChalanNo: '',
            ChalanDate: 'Chalan Date',
            alertTitle: '',
            alertMessage: '',
            confirmAlert: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            msgType: '',
            NoInternetModal: false,
            isPODatePickerVisible: false,
            isChalanDatePickerVisible: false,
            orderRecieveDate: '',
        }
        this.onLayout = this.onLayout.bind(this);

    }

    componentDidMount() {
        this.setState({ orderRecieveDate: moment(new Date(), "YYYY-MM-DD'T'HH:mm:ss").format('DD/MM/YYYY') })
        database().ref('JobType/').on('value', (snapshot) => {
            console.log(snapshot)
            let data1 = []
            snapshot.forEach(item => {
                var temp = { value: item.val().job_type };
                data1.push(temp);
            });
            console.log(data1)
            this.setState({ jobTypeData: data1 })
        });
       
    }

    onLayout(e) {
        this.setState({
            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
        }, () => {

        });
    }

    onLeftMenuPress() {
        this.setState({
            CompanyName: 'Select Company',
            job_type: '',
            GST_no: '',
            jobType: 'Select Job Type',
            quantity: '',
            modalVisible: false,
            PONo: '',
            PODate: 'P.O. Date',
            ChalanNo: '',
            ChalanDate: 'Chalan Date',
            WorkType: 'Select Work Type',
        })
        this.props.navigation.goBack(null);
    }

    onPressCancel() {
        this.setState({ modalVisible: !this.state.modalVisible }, () => {
            this.setState({
                CompanyName: 'Select Company',
                job_type: '',
                GST_no: '',
                jobType: 'Select Job Type',
                quantity: '',
                modalVisible: false,
                PONo: '',
                PODate: 'P.O. Date',
                ChalanNo: '',
                ChalanDate: 'Chalan Date',
                WorkType: 'Select Work Type',
            })
        });
    }

    onPressConfirm() {
        this.setState({ modalVisible: !this.state.modalVisible }, () => {
            this.setState({
                CompanyName: 'Select Company',
                job_type: '',
                GST_no: '',
                jobType: 'Select Job Type',
                quantity: '',
                modalVisible: false,
                PONo: '',
                PODate: 'P.O. Date',
                ChalanNo: '',
                ChalanDate: 'Chalan Date',
                WorkType: 'Select Work Type',
            })
        });
    }

    showPODatePicker = () => {
        this.setState({ isPODatePickerVisible: true });
    };

    hidePODatePicker = () => {
        this.setState({ isPODatePickerVisible: false });
    };

    handlePODatePicked = date => {
        let Fdate = moment(date).format('DD/MM/YYYY');
        this.setState({ PODate: Fdate, });
        this.hidePODatePicker();
    };

    showChalanDatePicker = () => {
        this.setState({ isChalanDatePickerVisible: true });
    };

    hideChalanDatePicker = () => {
        this.setState({ isChalanDatePickerVisible: false });
    };

    handleChalanDatePicker = date => {
        let Fdate = moment(date).format('DD/MM/YYYY');
        this.setState({ ChalanDate: Fdate });
        this.hideChalanDatePicker();
    };


    selectedCompanyNameChange(value, index, data) {
        this.setState({ CompanyName: value, GST_no: this.state.CompanyNameList[index].company_GST_no, PONo: this.state.CompanyNameList[index].PONo, PODate: this.state.CompanyNameList[index].PODate })
    }

    selectedJobTypeChange(value, index, data) {
        this.setState({ jobType: value })
    }

    selectedWorkTypeChange(value, index, data){
        this.setState({ WorkType: value })
    }

    submitPress() {
        if (this.state.jobType == 'Select Job Type') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Select Job Type',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.WorkType == 'Select Work Type') {
            this.setState({
                alertTitle: 'Alert',
                
                alertMessage: 'Please Select Work Type',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.quantity == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Enter Quantity',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.ChalanNo == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please enter Chalan Number',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.ChalanDate == 'Chalan Date') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Select Chalan Date',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else {
            
            let jobType = this.state.jobType
            let quantity = this.state.quantity
            let ChalanNo = this.state.ChalanNo
            let ChalanDate = this.state.ChalanDate
            let orderRecieveDate = this.state.orderRecieveDate
            let workType = this.state.WorkType

            database().ref('Orders/').child(ChalanNo.replace(/[^a-zA-Z0123456789 ]/g, "")).set({
                workType, jobType, quantity, ChalanNo, ChalanDate, orderRecieveDate
            }).then((data) => {
                //success callback
                console.log('data ', data)
                this.setState({
                    alertTitle: 'Alert',
                    alertMessage: 'New Order Added',
                    confirmAlert: false,
                    cancelButtonText: 'OK',
                    modalVisible: !this.state.modalVisible,
                    msgType: ''
                });
            }).catch((error) => {
                //error callback
                console.log('error ', error)
            })
        }
    }

    render() {
        return (
            <SafeAreaView onLayout={this.onLayout} style={{ flex: 1, backgroundColor: AppConstants.bacisColor }} forceInset={{ top: 'always', bottom: 'never' }}>
                <Header homeButton={false} header_title="Inward" backButton={true} onPressLeft={() => this.onLeftMenuPress()} />
                <ScrollView style={{ backgroundColor: AppConstants.backgroundColor }}>
                    <View style={{ flex: 1, backgroundColor: AppConstants.backgroundColor, }}>
                        
                        <Dropdown
                            textColor={'#1b1a1c'}
                            fontSize={AppConstants.infoSize}
                            fontFamily={AppConstants.mediumFont}
                            fontWeight={Platform.OS == 'android' ? 'normal' : null}
                            labelHeight={10}
                            label=''
                            containerStyle={styles.dropdown}
                            value={this.state.jobType}
                            data={this.state.jobTypeData}
                            inputContainerStyle={{ borderBottomColor: 'transparent', }}
                            onChangeText={(value, index, data) => this.selectedJobTypeChange(value, index, data)}
                        />
                        <Dropdown
                            textColor={'#1b1a1c'}
                            fontSize={AppConstants.infoSize}
                            fontFamily={AppConstants.mediumFont}
                            fontWeight={Platform.OS == 'android' ? 'normal' : null}
                            labelHeight={10}
                            label=''
                            containerStyle={styles.dropdown}
                            value={this.state.WorkType}
                            data={this.state.workTypeData}
                            inputContainerStyle={{ borderBottomColor: 'transparent', }}
                            onChangeText={(value, index, data) => this.selectedWorkTypeChange(value, index, data)}
                        />
                        <TextInput style={[styles.input,]}
                            placeholder='Quantity'
                            placeholderTextColor='rgb(152,152,152)'
                            keyboardType='number-pad'
                            onChangeText={(text) => {
                                let newText = '';
                                let numbers = '0123456789'
                                for (var i = 0; i < text.length; i++) {
                                    if (numbers.indexOf(text[i]) > -1) {
                                        newText = newText + text[i];
                                    }
                                }
                                this.setState({ quantity: newText })
                            }}
                            value={this.state.quantity}
                        />
                        <TextInput style={[styles.input,]}
                            placeholder='Chalan No.'
                            placeholderTextColor='rgb(152,152,152)'
                            onChangeText={(ChalanNo) => { this.setState({ ChalanNo: ChalanNo }) }}
                            value={this.state.ChalanNo}
                        />
                        <View style={[styles.dropdown, { paddingVertical: 10 }]}>
                            <Text onPress={this.showChalanDatePicker} style={{ fontSize: AppConstants.infoSize, fontFamily: AppConstants.mediumFont, }}>{this.state.ChalanDate}</Text>
                        </View>
                        <View style={{ marginVertical: 5, }}>
                            <TouchableOpacity style={[styles.buttonContainer,]} onPress={() => this.submitPress()} >
                                <Text style={styles.buttonText} >SUBMIT</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <BusyIndicator />
                <CustomAlert
                    confirmBox={this.state.confirmAlert}
                    cancelButtonText={this.state.cancelButtonText}
                    confirmButtonText={this.state.confirmButtonText}
                    modalVisible={this.state.modalVisible}
                    onPressCancel={() => this.onPressCancel()}
                    onPressConfirm={() => this.onPressConfirm()}
                    alertText={this.state.alertMessage}
                    alertTitle={this.state.alertTitle} />
                <DateTimePicker
                    isVisible={this.state.isPODatePickerVisible}
                    onConfirm={this.handlePODatePicked}
                    onCancel={this.hidePODatePicker}
                />
                <DateTimePicker
                    isVisible={this.state.isChalanDatePickerVisible}
                    onConfirm={this.handleChalanDatePicker}
                    onCancel={this.hideChalanDatePicker}
                />
            </SafeAreaView>
        )

    }
};

export default NewOrder; 
