
import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert, Image,Dimensions } from 'react-native';
import AppConstants from '../../Common/AppConstants';
const { width, height } = Dimensions.get('window');


const NoInternet = ({ modalVisible, }) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Modal
                animationType="slide"
                visible={modalVisible}
                onRequestClose={() => {
                    console.log('Modal has been closed.');
                }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../images/no-wifi.png')} style={{ width: width * 0.3, height: width * 0.3, resizeMode: 'contain' }} />
                    <Text style={{ marginVertical: 20, fontFamily: AppConstants.mediumFont, fontSize: AppConstants.buttonSize }}>No Internet Connection</Text>
                </View>
            </Modal>
        </View>
    );

}

export default NoInternet;