import React, { Component } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Switch,
    ScrollView,
    Image,
    Alert,
    ImageBackground,
    Dimensions
} from 'react-native';
import styles from './style';
import { Routes } from '../../navigation/Routes';
import {
    setAsyncAccessToken,
    setAsyncRefreshToken,
} from '../../Common/Helpers/SessionHelper';
const { width, height } = Dimensions.get('window');
import { Dropdown } from 'react-native-material-dropdown';
const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');
import AppConstants from '../../Common/AppConstants';


class SideMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            switchValue1: false,
            switchValue2: false,
            roleType: 'Select Role',
            roleTypeID: null,
            roleTypeData: [],
            ClientId: 'Praphullak'
        }
    }

    componentDidMount() {
    }

    onBackPress() {
        this.props.navigation.closeDrawer();
    }

    onPressLogout() {
        setAsyncAccessToken('');
        setAsyncRefreshToken('');

        this.props.navigation.replace(Routes.Login);
    }

    onPressAddCompany(){
        this.onBackPress()
        const { navigate } = this.props.navigation;
        navigate(Routes.AddCompany);
    }

    onPressAddJobType(){
        this.onBackPress()
        const { navigate } = this.props.navigation;
        navigate(Routes.AddJobType);
    }

    onPressNewOrder(){
        this.onBackPress()
        const { navigate } = this.props.navigation;
        navigate(Routes.NewOrder);
    }

    onPressCompletedOrder(){
        this.onBackPress()
        const { navigate } = this.props.navigation;
        navigate(Routes.CompleteOrder);
    }

    onPressAllInvoices(){
        this.onBackPress()
        const { navigate } = this.props.navigation;
        navigate(Routes.AllInvoices);
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{flex:1, marginHorizontal: 20}}>
                    <View style={styles.header}>
                        <View >
                            <Image source={require('../../images/logo.png')} style={{ marginBottom: 20, width: 100, height: 100, resizeMode: 'contain' }} />
                            <Text style={styles.headerTitle}>PK Engineering</Text>
                        </View>
                    </View>
                    {/* <TouchableOpacity onPress={() => this.onPressNewOrder()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>New Order</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressCompletedOrder()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>Complete Order</Text>
                    </TouchableOpacity> */}
                    <TouchableOpacity onPress={() => this.onPressAllInvoices()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>All Invoices</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressAddCompany()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>Add Company</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressAddJobType()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>Add Job Type</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onPressLogout()} style={[styles.touchbleMenu]}>
                        <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>Logout</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ marginHorizontal: 20}}>
                    <Text style={[styles.menutitle, { color: AppConstants.bacisColor }]}>Version 1.0</Text>
                </View>


            </SafeAreaView >
        );
    }
}


export default SideMenu;