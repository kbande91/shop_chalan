import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert, TextInput, Dimensions, FlatList } from 'react-native';
import AppConstants from '../../Common/AppConstants';
const { width, height } = Dimensions.get('window');


const AutoComplete = ({ selectRow, dataSource }) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', height: 300 }}>
            <FlatList
                style={{ backgroundColor: '#fff', marginVertical: 10, marginBottom: 40 }}
                data={dataSource}
                extraData={this.state}
                keyExtractor={_keyExtractor}
                renderItem={({ item, index }) => (
                    <TouchableOpacity onPress={() => selectRow(item)} style={{ marginVertical: 5, flexDirection: 'row', borderBottomColor: '#a1a1a1', borderBottomWidth: 0.5 }}>
                        <Text style={{ marginHorizontal: 10, fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, color: '#646464' }}>{item.value}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
}

function _keyExtractor(item, index) {
    return index.toString();
}



export default AutoComplete;