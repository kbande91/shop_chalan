import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert } from 'react-native';
import AppConstants from '../../Common/AppConstants';


const CustomAlert = ({ onPressConfirm, modalVisible, alertTitle, onPressCancel, alertText, cancelButtonText, confirmButtonText, confirmBox = false }) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    console.log('Modal has been closed.');
                }}>
                <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', paddingHorizontal: 20, flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: '#fff', borderRadius: 10, padding: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ textAlign: 'center', marginBottom: 10, color: '#323232', fontFamily: AppConstants.mediumFont, fontSize: AppConstants.inputSize }}>{alertTitle}</Text>
                        <Text style={{ textAlign: 'center', marginBottom: 30, color: '#323232', fontFamily: AppConstants.regularFont, fontSize: AppConstants.inputSize }}>{alertText}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            {confirmBox == true ?
                                <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7, paddingTop: 10, backgroundColor: AppConstants.bacisColor, borderRadius: 5, marginRight: confirmBox == true ? 10 : 0 }} onPress={onPressConfirm}>
                                    <Text style={{ color: '#fff', fontFamily: AppConstants.boldFont, fontSize: AppConstants.inputSize }}>{confirmButtonText}</Text>
                                </TouchableOpacity> : null
                            }
                            <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 7, paddingTop: 10, backgroundColor: AppConstants.bacisColor, borderRadius: 5, marginLeft: confirmBox == true ? 10 : 0 }} onPress={onPressCancel}>
                                <Text style={{ color: '#fff', fontFamily: AppConstants.boldFont, fontSize: AppConstants.inputSize }}>{cancelButtonText}</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    );

}

export default CustomAlert;