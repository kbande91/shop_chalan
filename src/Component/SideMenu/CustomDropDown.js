import React, { Component } from 'react';
import { Modal, Text, TouchableOpacity, View, Alert, TextInput, Dimensions, FlatList } from 'react-native';
import AppConstants from '../../Common/AppConstants';
const { width, height } = Dimensions.get('window');


const CustomDropDown = ({ modalVisible, onPressClose, placeholder, value = '', onChangeText, selectRow, dataSource }) => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    console.log('Modal has been closed.');
                }}>
                <View style={{ flex: 1, padding: 40, backgroundColor: 'rgba(0,0,0,0.8)',alignItems: 'center' }}>
                    <View style={{ marginTop: 20, alignItems: 'flex-end', width: width * 0.8, }}>
                        <TouchableOpacity style={{ height: 30 }} onPress={onPressClose}>
                            <Text style={{ color: '#fff', }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={{ marginTop: 20, }}>
                        <TextInput
                            clearButtonMode="always" 
                            value={value}
                            onChangeText={text => onChangeText(text)}
                            placeholder={placeholder}
                            style={{ height: 40, backgroundColor: '#fff', paddingHorizontal: 20, width: width * 0.8 }}
                        />
                        {value.length != 0 ?
                            <FlatList
                                style={{ backgroundColor: '#fff', marginVertical: 10, marginBottom: 40 }}
                                data={dataSource}
                                extraData={this.state}
                                keyExtractor={_keyExtractor}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity  onPress={() => selectRow(item)} style={{ marginVertical: 5, flexDirection: 'row', borderBottomColor: '#a1a1a1', borderBottomWidth: 0.5 }}>
                                        <Text style={{  marginHorizontal: 10, fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, color: '#646464' }}>{item.value}</Text>
                                    </TouchableOpacity>
                                )}
                            />
                            : null
                        }

                    </View>
                </View>
            </Modal>
        </View>
    );

}

function _keyExtractor(item, index) {
    return index.toString();
}



export default CustomDropDown;