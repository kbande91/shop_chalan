import {
    StyleSheet,
    Platform,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
 
export default StyleSheet.create({
    container: {
        flex: 1,
        borderRightColor: AppConstants.bacisColor,
        borderRightWidth: 3,
        backgroundColor: AppConstants.backgroundColor
    },
    header: {
        marginTop: 20,
        paddingVertical: 10,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom:20,
    },
    headerTitle: {
        fontFamily: AppConstants.blockFont,
        fontSize: AppConstants.titleSize,
        color: AppConstants.bacisColor
    },
    icon: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
    devider: {
        borderStyle: 'dashed',
        borderWidth: 0.5,
        borderRadius: 0.5,
        borderColor: '#353531'
    },
    touchbleMenu: {
        height: 60,
        justifyContent: 'center',
    },
    menuRow: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    menutitle: {
        fontFamily: AppConstants.mediumFont,
        fontSize: AppConstants.infoSize
    },
    dropdown: {
        height: Platform.OS == 'android' ? 40 : 35,
        width: 240,
        backgroundColor: '#ebebeb',
        color: '#1b1a1c',
        borderBottomWidth: 1,
        borderColor: "#989898",
        justifyContent: 'center',
        alignContent: 'center',
        marginBottom: 10,
        paddingHorizontal: 15, 
    },
})