import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
    Platform
} from 'react-native';
const { height, width } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
import styles from './style';
import { getShortDate } from '../../Common/Services/CommonAPIBL';

const ChalanItem = ({ section }) => {

    return (
        <View style={styles.hearderRow} >
            <View>
            <Text style={{ fontFamily: AppConstants.boldFont, fontSize: AppConstants.inputSize, marginVertical: 3 }}>Invoice No.    {section.InvoiceNo}</Text>
                <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.inputSize, marginVertical: 3 }}>Company Name     {section.PartyName}</Text>
                <Text style={{ fontFamily: AppConstants.regularFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Company GST No.    {section.PartyGST}</Text>
                <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3,}}>Date         {section.ChalanDate}</Text>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>CGST (6%): {section.CGSTAmount}</Text>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>SGST (6%): {section.SGSTAmount}</Text>
                </View>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Total TAX: {section.TotolTAX}</Text>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Total Amount: {section.GrandTotal}</Text>
                </View>
            </View>

        </View>
    )
}

export default ChalanItem;