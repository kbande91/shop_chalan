import {
    StyleSheet,
    Platform,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
var DeviceInfo = require('react-native-device-info');

export default StyleSheet.create({


    hearderRow: {
        marginVertical: 5,
        marginHorizontal: 10,
        padding: 10,
        borderColor: '#c8c8c8',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: '#e0e0e0',
        shadowColor: '#969696',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
    },
    buttonText: { 
        fontSize: 16, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: AppConstants.titleSize, 
        fontFamily: AppConstants.mediumFont,
    },
    dropdown: {
        borderRadius: 7, 
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        marginHorizontal:10,
        marginVertical: 10
    },
});
