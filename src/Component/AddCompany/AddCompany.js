import React, { Fragment } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Text,
    FlatList,
    TextInput,
    ScrollView,
    Keyboard
} from 'react-native';
import styles from './style';
import AppConstants from '../../Common/AppConstants';
import { SafeAreaView } from 'react-navigation';
import { Header } from '../common';
import { Routes } from '../../navigation/Routes';
import CustomAlert from '../SideMenu/CustomAlert';
import NetInfo from "@react-native-community/netinfo";
import database from '@react-native-firebase/database';
import moment from 'moment';

const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');
var DeviceInfo = require('react-native-device-info');
import DateTimePicker from "react-native-modal-datetime-picker";

class AddCompany extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
            company_Name: '',
            GST_no: '',
            modalVisible: false,
            alertTitle: '',
            alertMessage: '',
            confirmAlert: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            msgType: '',
            NoInternetModal: false,
            isPODatePickerVisible: false,
            PONo: '',
            PODate: 'P.O. Date',

        }
        this.onLayout = this.onLayout.bind(this);

    }

    componentDidMount() {


    }

    onLayout(e) {
        this.setState({
            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
        }, () => {

        });
    }

    onLeftMenuPress() {
        this.setState({

        })
        this.props.navigation.goBack(null);
    }

    onPressCancel() {
        this.setState({ modalVisible: !this.state.modalVisible, company_Name: '', GST_no: '', }, () => {
        });
    }

    onPressConfirm() {
        this.setState({ modalVisible: !this.state.modalVisible, company_Name: '', GST_no: '', }, () => {
        });
    }

    showPODatePicker = () => {
        this.setState({ isPODatePickerVisible: true });
    };

    hidePODatePicker = () => {
        this.setState({ isPODatePickerVisible: false });
    };

    handlePODatePicked = date => {
        let Fdate = moment(date).format('DD/MM/YYYY');
        this.setState({ PODate: Fdate, });
        this.hidePODatePicker();
    };

    submitPress() {
        if (this.state.company_Name == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Enter Company Name',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.GST_no == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Enter GST Number',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.PONo == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please enter P.O. Number',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else if (this.state.PODate == 'P.O. Date') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Select P.O. Date',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
            });
        }
        else {
            let company_Name = this.state.company_Name
            let company_GST_no = this.state.GST_no
            let POno = this.state.PONo
            let PODate = this.state.PODate
            database().ref('Company/').push({
                company_Name,
                company_GST_no,
                POno,
                PODate
            }).then((data) => {
                //success callback
                console.log('data ', data)
                this.setState({
                    alertTitle: 'Alert',
                    alertMessage: 'Company Added',
                    confirmAlert: false,
                    cancelButtonText: 'OK',
                    modalVisible: !this.state.modalVisible,
                    msgType: ''
                });
            }).catch((error) => {
                //error callback
                console.log('error ', error)
            })
        }
    }

    render() {
        return (
            <SafeAreaView onLayout={this.onLayout} style={{ flex: 1, backgroundColor: AppConstants.bacisColor }} forceInset={{ top: 'always', bottom: 'never' }}>
                <Header homeButton={false} header_title="Add Company" backButton={true} onPressLeft={() => this.onLeftMenuPress()} />
                <View style={{ flex: 1, backgroundColor: AppConstants.backgroundColor, }}>

                    <TextInput style={[styles.input,]}
                        placeholder='Company Name'
                        placeholderTextColor='rgb(152,152,152)'
                        onChangeText={(text) => this.setState({ company_Name: text })}
                        value={this.state.company_Name}
                    />
                    <TextInput style={[styles.input,]}
                        placeholder='GST No.'
                        placeholderTextColor='rgb(152,152,152)'
                        onChangeText={(GST_no) => this.setState({ GST_no: GST_no })}
                        value={this.state.GST_no}
                    />
                    <TextInput style={[styles.input,]}
                        placeholder='PO Number'
                        placeholderTextColor='rgb(152,152,152)'
                        onChangeText={(PONo) => { this.setState({ PONo: PONo }) }}
                        value={this.state.PONo}
                    />
                    <View style={[styles.dropdown, { paddingVertical: 10 }]}>
                        <Text onPress={this.showPODatePicker} style={{ fontSize: AppConstants.infoSize, fontFamily: AppConstants.mediumFont, }}>{this.state.PODate}</Text>
                    </View>
                    <View style={{ marginVertical: 5, }}>
                        <TouchableOpacity style={[styles.buttonContainer,]} onPress={() => this.submitPress()} >
                            <Text style={styles.buttonText} >SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <BusyIndicator />
                <CustomAlert
                    confirmBox={this.state.confirmAlert}
                    cancelButtonText={this.state.cancelButtonText}
                    confirmButtonText={this.state.confirmButtonText}
                    modalVisible={this.state.modalVisible}
                    onPressCancel={() => this.onPressCancel()}
                    onPressConfirm={() => this.onPressConfirm()}
                    alertText={this.state.alertMessage}
                    alertTitle={this.state.alertTitle} />
                <DateTimePicker
                    isVisible={this.state.isPODatePickerVisible}
                    onConfirm={this.handlePODatePicked}
                    onCancel={this.hidePODatePicker}
                />
            </SafeAreaView>
        )

    }
};

export default AddCompany; 
