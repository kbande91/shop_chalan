import {
    StyleSheet,
    Platform,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
var DeviceInfo = require('react-native-device-info');


export default StyleSheet.create({

    
    input: { 
        borderRadius: 7, 
        paddingVertical: 10,
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        color: '#1b1a1c', 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        fontSize: AppConstants.infoSize, 
        fontFamily: AppConstants.mediumFont,
        marginHorizontal:20,
        marginVertical: 10
    },
    buttonText: { 
        fontSize: 16, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: AppConstants.titleSize, 
        fontFamily: AppConstants.mediumFont,
    },

    buttonContainer: {
        borderRadius: 5, 
        paddingVertical: 7, 
        backgroundColor: AppConstants.bacisColor, 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginHorizontal:20,
        marginVertical: 10
    },
    dropdown: {
        borderRadius: 7, 
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        marginHorizontal:20,
        marginVertical: 10
    },
});
