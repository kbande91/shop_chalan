import React, { Fragment } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Text,
  FlatList
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Header } from '../common';
import styles from './style';
import { Routes } from '../../navigation/Routes';
import AppConstants from '../../Common/AppConstants';
import NetInfo from "@react-native-community/netinfo";
import NoInternet from '../SideMenu/NoInternet';
import ChalanItem from './ChalanItem';
import database from '@react-native-firebase/database';

const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');

import { setMasterData, getMasterData } from '../../Common/Helpers/SessionHelper';

class CompletedOrder extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      NoInternetModal: false,
      dataSource: []
    }
    this.onLayout = this.onLayout.bind(this);

  }

  componentDidMount() {

    this.internet_interval = setInterval(() => {
      // Your code
      NetInfo.fetch().then(state => {
        console.log("Is connected?", state.isConnected);
        if (state.isConnected == true) {
          this.setState({ NoInternetModal: false })
        }
        else {
          this.setState({ NoInternetModal: true })
        }
      });
    }, 5000);
    database().ref('Orders/').on('value', (snapshot) => {
      console.log(snapshot)
      let data1 = []
      snapshot.forEach(item => {
        var temp = {
          company_Name: item.val().company_Name,
          company_GST_no: item.val().company_GST_no,
          jobType: item.val().jobType,
          quantity: item.val().quantity,
          balance_quantity: item.val().balance_quantity,
          PONo: item.val().PONo,
          PODate: item.val().PODate,
          ChalanNo: item.val().ChalanNo,
          ChalanDate: item.val().ChalanDate,
          status: item.val().status,
          orderRecieveDate: item.val().orderRecieveDate
        };
        data1.push(temp);
      });
      console.log(data1)
      let data = []
      for (let i = 0; i < data1.length; i++) {
        if (data1[i].status == 'Completed') {
          data.push(data1[i])
        }
      }
      this.setState({ dataSource: data })
    });
  }

  componentWillUnmount() {
    clearInterval(this.internet_interval);
  }

  onLayout(e) {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }, () => {

    });
  }

  handleChalanClick(item) {
  }

  onLeftMenuPress() {

    this.props.navigation.goBack(null);
  }

  _keyExtractor = (item, index) => index.toString();

  _renderItem(item, index) {
    return (
      <ChalanItem section={item} UpdateChalanPress={(item) => this.handleChalanClick(item)} />
    );
  };

  render() {

    return (
      <SafeAreaView onLayout={this.onLayout} style={{ flex: 1, backgroundColor: AppConstants.bacisColor }} forceInset={{ top: 'always', bottom: 'never' }}>
        <Header homeButton={false} header_title="Complete Order" backButton={true} onPressLeft={() => this.onLeftMenuPress()} />
        <View style={{ flex: 1, backgroundColor: AppConstants.backgroundColor }}>
          <FlatList
            data={this.state.dataSource}
            extraData={this.state}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => this._renderItem(item, index)}
          />
        </View>
        <BusyIndicator />
        <NoInternet modalVisible={this.state.NoInternetModal} />
      </SafeAreaView>
    )
  }
};

export default CompletedOrder; 