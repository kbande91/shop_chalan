import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
    Platform
} from 'react-native';
const { height, width } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
import styles from './style';
import { getShortDate } from '../../Common/Services/CommonAPIBL';

const ChalanItem = ({ section }) => {

    return (
        <View style={styles.hearderRow} >
            <View>
                <Text style={{ fontFamily: AppConstants.boldFont, fontSize: AppConstants.inputSize, marginVertical: 3 }}>{section.company_Name}</Text>
                <Text style={{ fontFamily: AppConstants.regularFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Company Invoice No.    {section.ChalanNo}</Text>
                <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3, color: AppConstants.bacisColor }}>Total Quantity         {section.quantity}</Text>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Order Received: {section.orderRecieveDate}</Text>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>JobType: {section.jobType}</Text>
                </View>
                <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>P.O.No: {section.PONo}</Text>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>P.O. Date: {section.PODate}</Text>
                </View>
            </View>

        </View>
    )
}

export default ChalanItem;