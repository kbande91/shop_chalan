import {
    StyleSheet,
    Platform,
    Dimensions,
} from 'react-native';
import AppConstants from '../../Common/AppConstants';

export default StyleSheet.create({

    input: { 
        borderRadius: 10, 
        paddingVertical: 10,
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        color: '#1b1a1c', 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        fontSize: AppConstants.infoSize, 
        fontFamily: AppConstants.mediumFont,
    },
    capchainput: { 
        borderRadius: 5, 
        paddingVertical: 10,
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        color: '#1b1a1c', 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        fontSize: AppConstants.infoSize, 
        fontFamily: AppConstants.mediumFont,
    },
    buttonText: { 
        fontSize: 16, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: AppConstants.titleSize, 
        fontFamily: AppConstants.mediumFont,
    },

    container: { 
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center', 
        backgroundColor: '#EFFBFB', 
    },
    loginContainer: {
        flex: 1,  
        alignItems: 'center', 
    },
    buttonContainer: {
        borderRadius: 5, 
        paddingVertical: 7, 
        backgroundColor: AppConstants.bacisColor, 
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    
    
    LinkButton: { 
        color: AppConstants.bacisColor, 
        fontSize: 15,
        textAlign: 'center', 
        textDecorationLine: 'underline', 
        fontSize: AppConstants.titleSize, 
        fontFamily: AppConstants.mediumFont,
    },
    captchaIcon: { 
        margin: 1, 
        flexDirection: "column" 
    },


    loader: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
        //backgroundColor: "#EB9130",
    },
    dropdown: {
        height: Platform.OS == 'android' ? 40 : 35,
        backgroundColor: '#ebebeb',
        color: '#1b1a1c',
        borderBottomWidth: 1,
        borderColor: "#989898",
        justifyContent: 'center',
        alignContent: 'center',
        marginBottom: 10,
        paddingHorizontal: 15, 
    },
});
