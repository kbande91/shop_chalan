import {
    StyleSheet,
    Platform,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
var DeviceInfo = require('react-native-device-info');

export default StyleSheet.create({


    hearderRow: {
        marginVertical: 5,
        marginHorizontal: 10,
        padding: 10,
        borderColor: '#c8c8c8',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: '#e0e0e0',
        shadowColor: '#969696',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 5,
    },
    input: { 
        borderRadius: 7, 
        paddingVertical: 10,
        backgroundColor: '#ebebeb', 
        color: '#1b1a1c', 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        fontSize: AppConstants.infoSize, 
        fontFamily: AppConstants.mediumFont,
        paddingHorizontal: 15, 
        marginVertical: 10
    },
    buttonText: { 
        fontSize: 16, 
        color: '#fff', 
        textAlign: 'center', 
        fontSize: AppConstants.titleSize, 
        fontFamily: AppConstants.mediumFont,
    },
    buttonContainer: {
        borderRadius: 5, 
        paddingVertical: 7, 
        backgroundColor: AppConstants.bacisColor, 
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    dropdown: {
        borderRadius: 7, 
        backgroundColor: '#ebebeb', 
        paddingHorizontal: 15, 
        borderWidth: 1, 
        borderColor: "#dbe1ed", 
        marginHorizontal:20,
        marginVertical: 10
    },
});
