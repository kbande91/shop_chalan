import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Image,
    Platform
} from 'react-native';
const { height, width } = Dimensions.get('window');
import AppConstants from '../../Common/AppConstants';
import styles from './style';
import { getShortDate } from '../../Common/Services/CommonAPIBL';

const ChalanItem = ({ section, UpdateChalanPress }) => {

    return (
        <View style={styles.hearderRow} >
            <View>
                <Text style={{ fontFamily: AppConstants.boldFont, fontSize: AppConstants.inputSize, marginVertical: 3 }}>{section.company_Name}</Text>
                <Text style={{ fontFamily: AppConstants.regularFont, fontSize: AppConstants.infoSize, marginVertical: 3 }}>Company Invoice No.    {section.ChalanNo}</Text>
                <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3, color: AppConstants.bacisColor }}>Total Quantity         {section.quantity}</Text>
                <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.infoSize, marginVertical: 3, color: AppConstants.pendingOrder }}>Store Quantity         {section.balance_quantity}</Text>

            </View>
            <View style={{marginVertical: 5, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                    </View>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', backgroundColor: AppConstants.bacisColor, borderRadius: 7, paddingHorizontal: 10, paddingVertical: 4 }} onPress={UpdateChalanPress}>
                    <Text style={{ fontFamily: AppConstants.mediumFont, fontSize: AppConstants.inputSize, marginVertical: 3, color: '#fff' }}>Update Order</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default ChalanItem;