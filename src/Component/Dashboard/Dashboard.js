import React, { Fragment } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Text,
  FlatList
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { Header } from '../common';
import styles from './style';
import { Routes } from '../../navigation/Routes';
import AppConstants from '../../Common/AppConstants';
import NetInfo from "@react-native-community/netinfo";
import NoInternet from '../SideMenu/NoInternet';
import ChalanItem from './ChalanItem';
import database from '@react-native-firebase/database';
import { Dropdown } from 'react-native-material-dropdown';

const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');

import { setMasterData, getMasterData } from '../../Common/Helpers/SessionHelper';

class Dashboard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      NoInternetModal: false,
      dataSource: [],
      jobType: 'Select Job Type',
      jobTypeData: [],
      storeQuantity: '',
      totalInward: '',
      totalOutward: '',
      inwardArr: [],
      outwardArr: [],
      returnArr: []
    }
    this.onLayout = this.onLayout.bind(this);

  }

  componentDidMount() {

    this.internet_interval = setInterval(() => {
      // Your code
      NetInfo.fetch().then(state => {
        console.log("Is connected?", state.isConnected);
        if (state.isConnected == true) {
          this.setState({ NoInternetModal: false })
        }
        else {
          this.setState({ NoInternetModal: true })
        }
      });
    }, 5000);

    database().ref('JobType/').on('value', (snapshot) => {
      console.log(snapshot)
      let data1 = []
      snapshot.forEach(item => {
        var temp = { value: item.val().job_type };
        data1.push(temp);
      });
      console.log(data1)
      this.setState({ jobTypeData: data1 })
    });

    database().ref('Orders/').on('value', (snapshot) => {
      console.log(snapshot)
      let orderData = []
      snapshot.forEach(item => {
        var temp = {
          JobType: item.val().jobType,
          Quantity: item.val().quantity,
        };
        orderData.push(temp);
      });
      console.log(orderData)
      this.setState({ inwardArr: orderData })
    });
    database().ref('Invoices/').on('value', (snapshot) => {
      console.log(snapshot)
      let invoiceData = []
      snapshot.forEach(item => {
        var temp = {
          JobType: item.val().JobType,
          Quantity: item.val().dispatchQuantity,

        };
        invoiceData.push(temp);
      });
      this.setState({ outwardArr: invoiceData })
    });
    database().ref('ReturnInvoices/').on('value', (snapshot) => {
      console.log(snapshot)
      let returnArrData = []
      snapshot.forEach(item => {
        var temp = {
          JobType: item.val().JobType,
          Quantity: item.val().returnQuantity,

        };
        returnArrData.push(temp);
      });
      this.setState({ returnArr: returnArrData })
    });
  }

  componentWillUnmount() {
    clearInterval(this.internet_interval);
  }

  onLayout(e) {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
    }, () => {

    });
  }

  handleChalanClick(item) {
    const { navigate } = this.props.navigation;
    navigate(Routes.UpdateOrder, { orderDetails: item });
  }

  onLeftMenuPress() {
    this.props.navigation.openDrawer();
  }

  _keyExtractor = (item, index) => index.toString();

  _renderItem(item, index) {
    return (
      <ChalanItem section={item} UpdateChalanPress={() => this.handleChalanClick(item)} />
    );
  };

  inwardPress() {
    const { navigate } = this.props.navigation;
    navigate(Routes.NewOrder);
  }

  outwardPress() {
    const { navigate } = this.props.navigation;
    navigate(Routes.UpdateOrder);
  }

  selectedJobTypeChange(value, index, data) {
    this.setState({
      jobType: value
    }, () => {
      let total_Inward = 0
      let total_Outward = 0
      let return_job = 0
      for (var j = 0; j < this.state.inwardArr.length; j++) {
        if (this.state.inwardArr[j].JobType == value) {
          let quantity = Number(this.state.inwardArr[j].Quantity)
          total_Inward = total_Inward + quantity
        }
      }
      for (var i = 0; i < this.state.outwardArr.length; i++) {
        if (this.state.outwardArr[i].JobType == value) {
          let quantity = Number(this.state.outwardArr[i].Quantity)
          total_Outward = total_Outward + quantity
        }
      }
      for (var x = 0; x < this.state.returnArr.length; x++) {
        if (this.state.returnArr[x].JobType == value) {
          let quantity = Number(this.state.returnArr[x].Quantity)
          return_job = return_job + quantity
        }
      }
      let return_outward = total_Outward + return_job
      let store = total_Inward - return_outward

      this.setState({
        totalInward: total_Inward,
        totalOutward: return_outward,
        storeQuantity: store,
      })
    })
  }

  render() {

    return (
      <SafeAreaView onLayout={this.onLayout} style={{ flex: 1, backgroundColor: AppConstants.bacisColor }} forceInset={{ top: 'always', bottom: 'never' }}>
        <Header header_title="Dashboard" onPressLeft={() => this.onLeftMenuPress()} />
        <View style={{ flex: 1, backgroundColor: AppConstants.backgroundColor }}>
          <Dropdown
            textColor={'#1b1a1c'}
            fontSize={AppConstants.infoSize}
            fontFamily={AppConstants.mediumFont}
            fontWeight={Platform.OS == 'android' ? 'normal' : null}
            labelHeight={10}
            label=''
            containerStyle={styles.dropdown}
            value={this.state.jobType}
            data={this.state.jobTypeData}
            inputContainerStyle={{ borderBottomColor: 'transparent', }}
            onChangeText={(value, index, data) => this.selectedJobTypeChange(value, index, data)}
          />
          <View style={[styles.dropdown, { paddingVertical: 10 }]}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>Total Inward:</Text>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>{this.state.totalInward}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>Total Outward:</Text>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>{this.state.totalOutward}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>Store Quantity:</Text>
              <Text style={{ fontSize: AppConstants.inputSize, fontFamily: AppConstants.mediumFont, }}>{this.state.storeQuantity}</Text>
            </View>
          </View>
          <View style={{ marginHorizontal: 20, marginVertical: 10, flexDirection: 'row', justifyContent: 'space-between', }}>
            <View style={{ flex: 1, marginRight: 10 }}>
              <TouchableOpacity style={[styles.buttonContainer,]} onPress={() => this.inwardPress()} >
                <Text style={styles.buttonText} >Inward</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, marginLeft: 10 }}>
              <TouchableOpacity style={[styles.buttonContainer,]} onPress={() => this.outwardPress()} >
                <Text style={styles.buttonText} >Outward</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* {this.state.dataSource.length == 0 ?
            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
              <Text style={{ fontSize: AppConstants.titleSize, fontFamily: AppConstants.boldFont }}>No Record Found</Text>
            </View>
            : <FlatList
              data={this.state.dataSource}
              extraData={this.state}
              keyExtractor={this._keyExtractor}
              renderItem={({ item, index }) => this._renderItem(item, index)}
            />
          } */}
        </View>
        <BusyIndicator />
        <NoInternet modalVisible={this.state.NoInternetModal} />
      </SafeAreaView>
    )
  }
};

export default Dashboard; 