import React, { Fragment } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Text,
    FlatList,
    TextInput,
    ScrollView,
    Keyboard
} from 'react-native';
import styles from './style';
import AppConstants from '../../Common/AppConstants';
import { SafeAreaView } from 'react-navigation';
import { Header } from '../common';
import { Routes } from '../../navigation/Routes';
import CustomAlert from '../SideMenu/CustomAlert';
import NetInfo from "@react-native-community/netinfo";
import database from '@react-native-firebase/database';

const loaderHandler = require('react-native-busy-indicator/LoaderHandler');
const BusyIndicator = require('react-native-busy-indicator');
var DeviceInfo = require('react-native-device-info');

class AddJobType extends React.Component {

    constructor(props) {
        super(props)
        this.state = {

            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
            job_type: '',
            modalVisible: false,
            alertTitle: '',
            alertMessage: '',
            confirmAlert: false,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            msgType: '',
            NoInternetModal: false,
        }
        this.onLayout = this.onLayout.bind(this);

    }

    componentDidMount() {
        
       
    }

    onLayout(e) {
        this.setState({
            screenWidth: Dimensions.get('window').width,
            screenHeight: Dimensions.get('window').height,
        }, () => {
            
        });
    }

    onLeftMenuPress() {
        this.setState({
            job_type: ''
        })
        this.props.navigation.goBack(null);
    }

    onPressCancel() {
        this.setState({ modalVisible: !this.state.modalVisible }, () => {
            this.setState({
                job_type: ''
            })
        });
    }

    onPressConfirm() {
        this.setState({ modalVisible: !this.state.modalVisible }, () => {
            this.setState({
                job_type: ''
            })
        });
    }

    submitPress(){
        if (this.state.job_type == '') {
            this.setState({
                alertTitle: 'Alert',
                alertMessage: 'Please Enter Job Type',
                confirmAlert: false,
                cancelButtonText: 'OK',
                modalVisible: !this.state.modalVisible,
                msgType: ''
              });
        }
        else{
            let job_type = this.state.job_type
            database().ref('JobType/').push({
                job_type,
            }).then((data)=>{
                //success callback
                console.log('data ' , data)
                this.setState({
                    alertTitle: 'Alert',
                    alertMessage: 'Job Type Added',
                    confirmAlert: false,
                    cancelButtonText: 'OK',
                    modalVisible: !this.state.modalVisible,
                    msgType: ''
                  });
            }).catch((error)=>{
                //error callback
                console.log('error ' , error)
            })
        }
    }

    render() {
        return (
            <SafeAreaView onLayout={this.onLayout} style={{ flex: 1, backgroundColor: AppConstants.bacisColor }} forceInset={{ top: 'always', bottom: 'never' }}>
                <Header homeButton={false} header_title="Add Job Type" backButton={true} onPressLeft={() => this.onLeftMenuPress()} />
                <View style={{ flex: 1, backgroundColor: AppConstants.backgroundColor, }}>
                        
                        <TextInput style={[styles.input, ]}
                                placeholder='Job Type'
                                placeholderTextColor='rgb(152,152,152)'
                                onChangeText={(text) => this.setState({job_type: text})}
                                value={this.state.job_type}
                            />
                        
                        <View style={{ marginVertical: 5, }}>
                            <TouchableOpacity style={[styles.buttonContainer, ]} onPress={() => this.submitPress()} >
                                <Text style={styles.buttonText} >SUBMIT</Text>
                            </TouchableOpacity>
                        </View>
                </View>
                <BusyIndicator />
                <CustomAlert
                    confirmBox={this.state.confirmAlert}
                    cancelButtonText={this.state.cancelButtonText}
                    confirmButtonText={this.state.confirmButtonText}
                    modalVisible={this.state.modalVisible}
                    onPressCancel={() => this.onPressCancel()}
                    onPressConfirm={() => this.onPressConfirm()}
                    alertText={this.state.alertMessage}
                    alertTitle={this.state.alertTitle} />
            </SafeAreaView>
        )

    }
};

export default AddJobType; 
